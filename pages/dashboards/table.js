/**
 * Created by sujata on 8/30/2016.
 * Modified by ashish on 9/2/2016.
 *
 * A table element made of: rows and columns elements
 *
 */

var rows    = require('./table/rows'),
    columns = require('./table/columns'),
    Page    = require('astrolabe').Page;

module.exports = Page.create({

    tblDashboards: {
        get: function() {
            return $('#dashbList_container');
        }
    },

    title: {
        get: function () {
            return $('#dashbList_title').getText();
        }
    },

    rows: {
        get: function () {
            return rows.all;
        }
    },

    row: {
        value: function (rowNumber) {
            return rows.byNumber(rowNumber);
        }
    },

    columns: {
        get: function () {
            return columns.all;
        }
    },

    column: {
        value: function (columnName) {
            return columns.byName(columnName);
        }
    }
});