/**
 * Created by Ashish Raina on 9/6/2016.
 *
 * refer: https://github.com/angular/protractor/blob/master/docs/webdriver-vs-protractor.md
 */

var Page = require('astrolabe').Page,
    resultsTable = require('./table');

module.exports = Page.create({

    /**
     * return pre-defined Page Sub-Components
     * @return {Object} various Page UI Component Objects
     */
    url: {value: '/#/records'},
    resultsTable: {get: function () {return resultsTable}},
    /**
     * Custom Assertion
     * @return {boolean}
     */
    isAllRowsCount: {
        value: function(expectedCount) {
            this.resultsTable.rows.then(function (rowsAll) {
                return rowsAll.length === expectedCount;
            });
        }},

    /**
     * Pagination Controls
     * @return {promise ElementFinder} Protractor elements
     */
    lblTotalResultsCount: { get: function () { return $('.pager_links span b:nth-child(2)');}},
    lblPageResultsRange:  { get: function () { return $('.pager_links span b:nth-child(1)');}},
    eleNextPage:     { get: function () { return $('.btn-group .fa-chevron-right');}},
    elePreviousPage: { get: function () { return $('.btn-group .fa-chevron-left')  // resolves promise, returns element!!
        // return  this.findElement(this.by.css('[class="fa fa-chevron-left"]'));    // returns a promise
    }},

    btnSearch: {get: function () {return $('.keywordSearchButton');}},
    txtSearch: {get: function () {return $('.input-group input');}},
    btnDropdown: {get: function () {return $('.input-group-addon.btn.btn-default.advanceSearchButton');}},
    advanceSearchContainer: {get: function () {return $('.advanceSearchContainer');}},
    advanceSearchHeader: {get: function () {return $('.searchHeader div');}},
    btnCrossAdvanceSearch: {get: function () {return $('.searchHeader span');}},
    txtKeyword: {get: function () {return $('.searchCriteria');}},
    btnClear: {get: function () {return $('.btn.btn-default.btn-sm.ng-binding');}},

    // Advance search page
    patientTextAdvance: {get: function () {return $('.col-xs-12');}},
    patientNameTextAdvance: {get: function () {return $('[for=patientName]');}},
    InputPatientNameAdvance: {get: function () {return $('#patientName');}},
    patientIdTextAdvance: {get: function () {return $('[for=patientId]');}},
    InputPatientIdAdvance: {get: function () {return $('#patientId');}},
    patientDobTextAdvance: {get: function () {return $('[for=dob]');}},
    monthDropdown: {get: function () {return $('[data-ng-model=month]');}},
    Inputday: {get: function () {return $('[data-ng-model=day]');}},
    Inputyear: {get: function () {return $('[data-ng-model=year]');}},
    genderDropdown: {get: function () {return $('#gender');}},
    patientGenderTextAdvance: {get: function () {return $('[for=gender]');}},
    orderNameTextAdvance: {get: function () {return $('[for=orderName]');}},
    InputOrderNameAdvance: {get: function () {return $('#orderName');}},
    orderIdTextAdvance: {get: function () {return $('[for=orderId]');}},
    InputOrderIdAdvance: {get: function () {return $('#orderId');}},
    recordTypeTextAdvance: {get: function () {return $('[for=recordType]');}},
    recordTypeDropdown: {get: function () {return $('#recordType');}},
    recordTypeDropdownAll: {get: function () {return $('#recordType .ng-binding');}},
    statusTextAdvance: {get: function () {return $('[for=status]');}},
    statusDropdown: {get: function () {return $('#status');}},
    statusPleaseSelect: {get: function () {return $('#status .ng-binding');}},
    orderDateFromTextAdvance: {get: function () {return $('[for=orderDateFrom]');}},
    InputOrderDateFrom: {get: function () {return $('#orderDateFrom');}},
    orderDateToTextAdvance: {get: function () {return $('[for=orderDateTo]');}},
    InputOrderDateTo: {get: function () {return $('#orderDateTo');}},
    modalityTextAdvance: {get: function () {return $('[for=modality]');}},
    modalityDropdown: {get: function () {return $('#modality');}},
    modalityPleaseSelect: {get: function () {return $('#modality .ng-binding');}},
    orderingPhysicianTextAdvance: {get: function () {return $('[for=orderingPhysician]');}},
    InputOrderingPhysician: {get: function () {return $('#orderingPhysician');}},
    performingLocationTextAdvance: {get: function () {return $('[for=performingLocation]');}},
    InputPerformingLoaction: {get: function () {return $('#performingLocation');}},
    uploadedByTextAdvance: {get: function () {return $('[for=uploadedBy]');}},
    InputUploadedBy: {get: function () {return $('#uploadedBy');}},
    checkboxBulkUpload: {get: function () {return $('.checkCss');}},
    textBulkUpload: {get: function () {return $('.checkCss');}},
    btnAdvanceSearch: {get: function () {return  $('.recordFilter.text-right.flip input');}},
    btnClearAll: {get: function () {return $('.btn.btn-default.clearAllButton');}},
    noResultFound: {get: function () {return $('.mt15.noRecords div h4');}},
    errorText: {get: function () {return $('.errorText');}},
    monthDropdownList: {get: function () {return $$('[data-ng-bind=monthName]');}},
    genderDropdownList: {get: function () {return $$('[ng-repeat="option in serverConfig.availableGenderList"]');}},
    testTypeDropdownList: {get: function () {return $$('[id^=category]');}},
    testStatusDropdownList: {get: function () {return $$('[ng-repeat="option in serverConfig.availableRecordStatuses"]');}},
    modalityDropdownList: {get: function () {return $$('[ng-repeat="option in serverConfig.availableModalities"]');}},




    genderlistText:{
        get: function () {
            var list = [];
            return this.genderDropdownList.then(
                function (elements) { elements.forEach(
                    function (eachElement) { eachElement.getText().then(
                        function (text) {
                            list.push(text.trim());
                        });
                    });
                    return list;
                });
        }
    },

    recordStatuslistText:{
        get: function () {
            var list = [];
            return this.testStatusDropdownList.then(
                function (elements) { elements.forEach(
                    function (eachElement) { eachElement.getText().then(
                        function (text) {
                            list.push(text.trim());
                        });
                    });
                    return list;
                });
        }
    },


    testTypelistText:{
        get: function () {
            var list = [];
            return this.testTypeDropdownList.then(
                function (elements) { elements.forEach(
                    function (eachElement) { eachElement.getText().then(
                        function (text) {
                            list.push(text.trim());
                        });
                    });
                    return list;
                });
        }
    },
    modalityTypelistText:{
        get: function () {
            var list = [];
            return this.modalityDropdownList.then(
                function (elements) { elements.forEach(
                    function (eachElement) { eachElement.getText().then(
                        function (text) {
                            list.push(text.trim());
                        });
                    });
                    return list;
                });
        }
    },


    txtKeywordVisible:{
        get: function() {
            var ele = this.txtKeyword;
            return ele.isPresent().then(
                function (result) {
                    if(result) {
                        return ele.isDisplayed();
                    }
                    else {
                        return false;
                    }
                }
            );
        }
    },

    listText2:{
        get: function () {
            var list = [];
            return this.monthDropdownList.then(
                function (elements) { elements.forEach(
                    function (eachElement) { eachElement.getText().then(
                        function (text) {
                            list.push(text);
                        });
                    });
                    return list;
                }
            );
        }
    },


    // Pagination Controls - Custom JS elements
    btnNextPage: {
        get: function () {
            return (this.btnFromElement(this.eleNextPage)); // Constructs btn with additional properties
        }
    },

    btnPreviousPage: {
        get: function () {
            return (this.btnFromElement(this.elePreviousPage)); // Constructs btn with additional properties
        }
    },

    btnFromElement: {
        value: function (btnElement) {
            var page = this; // this => resultsPage
            return {
                isDisabled: function () { return page.btnIsDisabledFromElement(btnElement); },  // this is NOT=> resultsPage inside method
                click: function () { btnElement.click(); },
            };
        }
    },

    btnIsDisabledFromElement: {
        value: function (btnElement) {
            return btnElement.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
                return (fontColor === expectedFontColor);
            });
        }
    },


    getCurrentResultsRange: {value: function () {return this.lblPageResultsRange.getText(); }},
    getTotalResultsCount: { value: function () {return this.lblTotalResultsCount.getText(); }},

    isBtnPreviousPageDisabled: {
        get: function () {
            this.btnPreviousPage.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(152, 152, 152, 1)'; // Grey font for inactive buttons
                return (fontColor === expectedFontColor);
            });
        }
    },

    btnIsEnabledFromElement: {
        value: function (btnElement) {
            return btnElement.getCssValue('color').then(function (fontColor) {
                var expectedFontColor = 'rgba(255, 255, 255, 1)';
                return (fontColor === expectedFontColor);
            });
        }
    },


    gotoNextPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.eleNextPage.click();
                loop -= 1;
            }
        }
    },
    gotoPreviousPage: {
        value: function (loop) {
            loop = loop === undefined ? 1 : loop;
            while (loop) {
                this.elePreviousPage.click();
                loop -= 1;
            }
        }
    }
});