/**
 * Created by sujata on 9/28/2016.
 */

var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3298.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('verify drop down list of month: MI-3298', function () {
        minerva.results.btnDropdown.click();
        minerva.results.monthDropdown.click();
        expect(minerva.results.listText2).toEqual(testData.monthList)
	});

    afterEach(function () {
        minerva.logout();
	});
});