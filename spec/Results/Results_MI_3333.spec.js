/**
 * Created by sujata on 10/7/2016.
 */
var minerva = require('../../pages/minerva'),
    testData = require('./Results_MI_3333.json');

describe('results table', function() {
    beforeEach(function () {
        minerva.login(browser.params.userInternal);
        minerva.navbar.tab('Results').then(function (recordsTab) {
            recordsTab.visit();
            columns = minerva.results.resultsTable.columns;
        });
    });

    it('non-existing/invalid Order id: MI-3333', function () {
        minerva.results.btnDropdown.click();
        minerva.results.InputOrderIdAdvance.sendKeys(testData.nonExistingData)
        minerva.results.btnAdvanceSearch.click();
        expect(minerva.results.txtKeyword.getText()).toEqual('Order ID: '+testData.nonExistingData)
        expect(minerva.results.btnClear.isDisplayed()).toBeTruthy();
        expect(minerva.results.noResultFound.getText()).toEqual(testData.noResultFound)
    });
    afterEach(function () {
        minerva.logout();
    });
});